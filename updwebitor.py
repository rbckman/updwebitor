#!/usr/bin/env python
# -*- coding: utf-8 -*-

##----------UPDWEBITOR v.022-------------------------
##----------the web editor for updweb-------------------
##------------------------------------------------------
##----------check updwebconfig.py for setup------------
##------------------------------------------------------
##----------wHATS your story?-----------------------------
##-----------Explorer? Adventurer? the inventor?...........
##--------Tell the world aboot it------------------------------------------
##-------This is free software and may it be used and remixed, aight!------
##-------------------------------------------------------------------------
##--------Beginz----------------------May-2015-rbckman-www.robinbackman.com

import curses
import time
import os
import locale
import sys
import re
import config

##--------Set locale----------------------probably not needed?

locale.setlocale(locale.LC_ALL,"fi_FI.UTF-8")

##-------Some strings and ints------------------

global x, y, line, dispx, dispy, event, charcount, storyname, line, width, height

x = 0
y = 0
char = []
chary = []
line = [[]]
event = ""
charcount = 0
countinlines = 0
lines = ''
dispx = 3
dispy = 3
storyname = ""
rows, columns = os.popen('stty size', 'r').read().split()
width = int(columns) - 30
storiespath = config.localstoriespath
imagepath = config.localimagepath
height = config.height
server = config.server
serverstoriespath = config.serverstoriespath
serverimagepath = config.serverimagepath
localimagepath = config.localimagepath
localstoriespath = config.localstoriespath
updwebpath = config.updwebpath

##-----Fire up the curses engine------------

def cursessetup():
    curses.noecho()
    curses.curs_set(0)
    screen.keypad(1)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
    screen.scrollok(1)

##--------Load excisting story--------------------

def openstory():
    os.chdir(storiespath) ##load stories and sort em
    stories= filter(os.path.isfile, os.listdir(storiespath))
    stories.sort(key=os.path.getmtime)
    stories.reverse()

    screen.clear()
    r = 0
    storyname = ""
    allowedchr = ['1','2','3','4','5','6','7','8','9','0','q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M']

    ##--------Select a story---------------
    while True:
        screen.addstr(1, 3, "write a new story:                                ", curses.color_pair(3))
        if len(stories) != 0:
            screen.addstr(3, 3, "or open a recent story (up)(down)                 ", curses.color_pair(2))
            screen.addstr(4, 3, "recent: " + stories[r] + "                                       ", curses.color_pair(2))
        event = screen.getch()
        if event == curses.KEY_UP:
            if r > 0:
                r = r - 1
                screen.addstr(4, 3, "recent: " + stories[r] + "                               ", curses.color_pair(2))
        elif event == curses.KEY_DOWN:
            if r < len(stories) - 1:
                r = r + 1
                screen.addstr(4, 3, "recent: " + stories[r] + "                               ", curses.color_pair(2))
        elif event == 27:
            exit()
        elif event == 10 and len(stories) != 0:

            ##-------------------Load story from file--------------
            del char[:]
            line[:] = [[]]
            f = open(storiespath + stories[r], 'r')
            lines = f.readlines()
            f.close()
            p = 0
            thedots = ''
            jump = 0
            for i in lines:
                ch = 0
                for a in i:
                    ch = ch + 1
                    if a != '\xc3':
                        enterkey = a.replace('\n', '|')
                        line[p].append(thedots + enterkey)
                    thedots = ''
                    if a == '\xc3': ##<---------Hacky thing to load åäö right
                        thedots = a ##          an å,ä,ö has two bytes thats why I need to combined them here..
                    if a == '\n':
                        line.append([])
                        ch = 0
                        p = p + 1
                    if ch > width and a == ' ':  
                        line.append([])
                        ch = 0
                        p = p + 1
                    if ch > width + 20:
                        line.append([])
                        ch = 0
                        p = p + 1
            return stories[r] ##<---return the story name
            break
        elif event == 32:
            screen.addstr(2, 3, 'Sorry mate! no spaces in the storyname.', curses.color_pair(2))
        elif event == 10 and len(stories) == 0:
            screen.addstr(2, 3, 'Please write something...', curses.color_pair(2))
    ##---------Write a new story---------------
        else:
            if chr(event) in allowedchr:
                try:
                    storyname = storyname + (chr(event))
                except:
                    continue
            screen.clear()
            screen.addstr(1, 3, "write a new story:                                      ", curses.color_pair(3))
            screen.addstr(1, 22, storyname)
            while True:
                event = screen.getch()
                if event == 27:
                    exit()
                elif event == 10:
                    if storyname in stories:
                        screen.addstr(2, 3, 'Sorry mate! ' + storyname + ' is already written! try another', curses.color_pair(2))
                        storyname = ''
                        break
                    if len(storyname) > 0:
                        line[:] = [[]]
                        return storyname
                    screen.addstr(2, 3, 'Please write something...', curses.color_pair(2))
                elif event == 127 or event == curses.KEY_BACKSPACE:
                    if len(storyname) > 0:
                        storyname = storyname[:-1]
                    screen.addstr(1, 22, storyname + '                                                             ')
                elif event == 32:
                    screen.addstr(2, 3, 'Sorry mate! no spaces in the storyname.', curses.color_pair(2))
                else:
                    if chr(event) in allowedchr:
                        try:
                            storyname = storyname + (chr(event))
                        except:
                            continue
                        screen.addstr(1, 22, storyname + '                                                             ')

##---------Saving story-----------------------------------------

def save():
    f = open(storiespath + storyname, 'wb')
    for i in line:
        for a in i:
            if a != '\n':
                if a != '':
                    enterkey = a.replace('|', '\n')
                    f.write(''.join(enterkey))
    f.close()
    screen.addstr(1, 3, "your story has been saved....                                         ")

##---------Delete story--------------------------------------

def delete(storyname):
    screen.addstr(1, 3, "Are you sure you want to delete this story? (y)es or (n)o?                 ", curses.color_pair(2))
    while True:
        event = screen.getch()
        if event  == ord('y'):
            screen.clear()
            screen.addstr(1, 3, 'Removing your shitty story...                                                ', curses.color_pair(2))
            screen.refresh()
            os.system('rm %s%s' % (localstoriespath, storyname))
            os.system('ssh %s -t rm %s%s' % (server, serverstoriespath, storyname))
            screen.clear()
            screen.refresh()
            screen.addstr(1, 3, 'Story removed locally and on the server!', curses.color_pair(2))
            screen.addstr(2, 3, "Now running updweb on server...", curses.color_pair(3))
            screen.move(3, 3)
            screen.refresh()
            os.system('ssh %s -t %s' % (server, updwebpath))
            screen.clear()
            storyname = openstory()
            x = 0
            y = 0
            dispx = 3
            dispy = 3
            refresh()
            break
        else:
            refresh()
            break

##---------Exit--------See you later aligator-------------------

def exit():
    screen.addstr(1, 3, "Hastala vista baby!")
    curses.nocbreak()
    screen.keypad(0)
    curses.echo()
    curses.endwin()
    quit()

##----------Insert Images---------------------------------------

def insertpic():
    screen.clear()
    os.chdir(imagepath)
    images= filter(os.path.isfile, os.listdir(imagepath))
    images.sort(key=os.path.getmtime)
    images.reverse()
    r = 0
    screen.addstr(1, 3, "choose (up) (down) image to upload.                                          ")
    screen.addstr(2, 3, "image: " + images[r])
    while True:
        event = screen.getch()
        if event == curses.KEY_UP:
            if r >= 0:
                r = r - 1
                screen.addstr(2, 3, "image: " + images[r] + "                       ")
        elif event == curses.KEY_DOWN:
            if r < len(images) - 1:
                r = r + 1
                screen.addstr(2, 3, "image: " + images[r] + "                        ")
        elif event == 10:
            description = ''
            screen.addstr(1, 3, "Write a description for the image(or dont):                              ")
            screen.addstr(2, 3, ":                                                                        ")
            while True:
                event = screen.getch()
                if event == 10:
                    imagestring = '!' + description + '!' + '(' + images[r] + ')'
                    return imagestring
                    break
                elif event == 127 or event == curses.KEY_BACKSPACE:
                    if len(description) > 0:
                        description = description[:-1]
                    screen.addstr(2, 4, description + '                                                             ')
                else:
                    try:
                        description = description + (chr(event))
                    except:
                        continue
                    screen.addstr(2, 4, description + '                                                             ')
        else:
            refresh()
            break

##------------Update to the webz---------------------------------------

def updweb(storyname):
    screen.clear()
    screen.addstr(1, 3, "uploading your story.. with secure transfer(ssh)", curses.color_pair(2))
    imagecount = 0
    imagename = ''
    imagelist = []
    screen.refresh()
    ## search for images to upload
    if os.path.isfile(localstoriespath+storyname):
        f = open(localstoriespath+storyname)
        for line in f:
            image = re.findall(r'\!.*?\!\(.*?\)', line) #find the image
            if image:
                imagecount = imagecount + 1
                imagename = re.findall('\((.*)\)', line)
                imagelist.append(localimagepath + imagename[0])
    if imagecount != 0:
        screen.addstr(2, 3, "Images to upload: " + str(imagecount), curses.color_pair(3))
        screen.addstr(3, 3, "Hold your horses! Uploading them images...", curses.color_pair(2))
        screen.move(4, 3)
        screen.refresh()
        os.system('scp %s "%s:%s" ' % (' '.join(imagelist), server, serverimagepath))
        screen.clear()
        screen.refresh()
        screen.addstr(1, 3, 'Images uploaded!', curses.color_pair(3))
        screen.addstr(3, 3, 'Uploading story...', curses.color_pair(2))
        screen.addstr(2, 3, 'If you want to do this faster, consider to make an ssh-key', curses.color_pair(3))
    screen.move(4, 3)
    screen.refresh()
    os.system('scp %s "%s:%s" ' % (localstoriespath + storyname, server, serverstoriespath))
    screen.clear()
    screen.refresh()
    screen.addstr(1, 3, 'Story file safely transfered!', curses.color_pair(2))
    screen.addstr(2, 3, "Now running updweb on server...", curses.color_pair(3))
    screen.move(3, 3)
    screen.refresh()
    os.system('ssh %s -t %s' % (server, updwebpath))
    screen.clear()
    return


##--------The Refreshing & Writing lab------------------------------------------------------------------------

def refresh():
    rows, columns = os.popen('stty size', 'r').read().split()
    width = int(columns) - 170
    dims = screen.getmaxyx()
    screen.clear()
    e = 0
    #This is the write process
    for p in line:
        if dispy + e > 0:
            if dispy + e < int(rows) - 3:
                screen.addstr(e + dispy, dispx, ''.join(p), curses.color_pair(3))
        e = e + 1
    if y + dispy > 0:
        if x + dispx > 0:
            if x + dispx < int(columns) - 3:
                screen.insstr((y + dispy), (x + dispx), "<", curses.A_REVERSE)
    if charcount >= 0:
        screen.addstr(1, 3, '-:::' + storyname + ':::-   press (Ctrl + a) if u need anything', curses.color_pair(2))
    ##Debug mode---------------------------------------------------------------------Dive into debug mode by uncommenting this groove stuff------
    ##if charcount >= 0:
        ##screen.addstr(25, 3, "p presses: " + str(charcount) + " event: " + str(event) + ' x: ' + str(x) + ' y: ' + str(y), curses.color_pair(1))
        ##screen.addstr(26, 3, 'how many lines: ' + str(len(line)))
        ##screen.addstr(28, 3, str(line))
        ##screen.addstr(27, 3, 'screen dimensions: ' + str(dims))

##------------(The main loop)><listens for inputs)------------------


screen = curses.initscr()
cursessetup()
##check if stories path exists
if os.path.isdir(config.localstoriespath) == False:
    screen.addstr(2, 3, "No stories path found. Do ya wanna create " + config.localstoriespath + " now? y/n")
    while True:
        event == screen.getch()
        if event == ord('n'):
            exit()
        else:
            try:
                os.system('mkdir ' + config.localstoriespath)
            except:
                screen.addstr(3, 3, "Couldnt do it. Try to create " + config.localstoriespath + " manually")
            break

storyname = openstory()
refresh()

while True:
    event = screen.getch()
    if event == curses.KEY_UP:
        if y >= 1:
            y = y - 1
            if x >= len(line[y + 1]):
                x = len(line[y])
        if y > height - 1:
            dispy = dispy + 1
        refresh()
    elif event == curses.KEY_DOWN:
        if len(line) - 1 > y:
            y = y + 1
            if x >= len(line[y-1]):
                x = len(line[y])
            if y == len(line):
                x = len(line[y])
            if y > height:
                dispy = dispy - 1
        refresh()
    elif event == curses.KEY_LEFT:
        if x >= 1:
            x = x - 1
        if x < 1:
            if y > 0:
                y = y - 1
                x = len(line[y])
                if y > height - 1:
                    dispy = dispy + 1
        refresh()
    elif event == curses.KEY_RIGHT:
        if len(line[y]) > x:
            x = x + 1
        if y < len(line) - 1:
            if x >= len(line[y]):
                x = 0
                y = y + 1
                if y > height:
                    dispy = dispy - 1
        refresh()
    elif event == 10:##<-------------------------ENTER KEY-------------------
        line.insert(y + 1, [])      ##<----------insert new line--------------------
        for i in line[y][x:]:       ##<----------check if theres letter on the left side
            line[y + 1].append(i)   ##<----------Append the letters to the next line
        del line[y][x:]             ##<----------Delete the old ones.................
        line[y].insert(x, '|')      ##<----------insert the enter key mark '|' ...
        x = 0
        y = y + 1
        if y > height:
            dispy = dispy - 1
        refresh()
    elif event == 165:
        charcount = charcount + 1
        line[y].insert(x, 'å')
        refresh()
    elif event == 164:
        charcount = charcount + 1
        line[y].insert(x, 'ä')
        refresh()
    elif event == 182:
        charcount = charcount + 1
        line[y].insert(x, 'ö')
        refresh()
    elif event == 1: ##<---------------CTRL + A ------MENU--------------
        while True:
            screen.addstr(1, 3, "(s)ave (u)pload (p)icture (h)elp (n)ew (o)pen (d)elete (q)uit            ", curses.color_pair(2))
            event = screen.getch()
            if event == ord('s'):
                save()
                break
            elif event == ord('u'):
                save()
                updweb(storyname)
                screen.addstr(1, 3, "Story is on the intrawebz, rick'n'roll!")
                break
            elif event == ord('q'):
                exit()
            elif event == ord('h'):
                screen.clear()
                screen.addstr(2, 3, 'Headers: #big ##medium ###small', curses.color_pair(3))
                screen.addstr(3, 3, 'Links:   [Robins firma](http://robinsfirma.com)', curses.color_pair(3))
                screen.addstr(4, 3, 'List:    *thing one *thing two *thing three', curses.color_pair(3))
                screen.addstr(5, 3, 'Code:    `print hello`', curses.color_pair(3))
                screen.addstr(7, 3, 'more help at www.robinbackman.com/updweb', curses.color_pair(3))
                break
            elif event == ord('p'):
                imagestring = insertpic()
                for i in imagestring:
                    line[y].append(i)
                x = x + len(imagestring)
                refresh()
                break
            elif event == ord('o') or event == ord('n'):
                storyname = openstory()
                x = 0
                y = 0
                dispx = 3
                dispy = 3
                refresh()
                break
            elif event == ord('d'):
                delete(storyname)
                break
            else:
                screen.addstr(1, 3, "go on writing...                                                    ")
                break
    elif event == 127 or event == curses.KEY_BACKSPACE: ##event 127 is workin on my phone while key_backspace for my laptop
        if x < 1 and y > 0:
            if len(line[y]) < 1 :
                del line[y]
            y = y - 1
            x = len(line[y])
            if y > height - 1:
                dispy = dispy + 1
        if x >= 1:
            x = x - 1
            try:
                line[y].pop(x)
            except:
                continue
            refresh()
            ##Here is the backspace drag function that drags a character up to the next line if there's no enter
            try:
                if line[y][-1] != '|':
                    for i in range(y + 1, len(line)):
                        try:
                            if line[i - 1][-1] == '|':
                                break
                        except:
                            continue
                        if len(line) > y:
                            try:
                                line[i - 1].append(line[i][0])
                            except:
                                continue
                            try:
                                line[i].pop(0)
                            except:
                                continue
                        if line[i] == []:
                            del line[i]
            except:
                continue
        refresh()
    elif event == 27: ##<------------------------ESC KEY----------------
        screen.addstr(1, 3, '(s)ave it for later or (q)uit? or (c)ontinue writing'                                       )
        while True:
            event = screen.getch()
            if event == ord('s'):
                save()
                break
            elif event == ord('q'):
                exit()
            else:
                screen.addstr(1, 3, "Good, go on writing!                                                     ")
                break
    else:
        try:
            if chr(event) != '\xc3': ##<<-------HACK for being able to have åäö
                line[y].insert(x, chr(event))
        except:
            screen.addstr(1, 3, "No such a key supported, sorry!                                          ")
            continue
        x = x + 1
        charcount = charcount + 1

        ##------------Jumpin lines---------------Should do function of this--------------------------
        ##------Line jump-----Maybe the hardest part---make the line appear on next line if its too long----------
        if x < len(line[y]):
            for i in range(y, len(line)):
                if len(line[i]) > width and ' ' in line[i][-2] or len(line[i]) > width + 20:
                    try:
                        line[i + 1].insert(0, line[i][-1])
                    except:
                        line.append([])
                        line[i + 1].insert(0, line[i][-1])
                    line[i].pop(-1)
        if x == len(line[y]):
            jump = 0         
            if len(line[y]) > width and ' ' in line[y][-2] or len(line[y]) > width + 20:
                line.append([])
                line[y + jump + 1].insert(0, line[y + jump].pop(-1))
                jump = jump + 1
                x = 1
                y = y + 1
                if y > height:
                    dispy = dispy - 1
        refresh()
curses.endwin()
