#!/usr/bin/env python
# -*- coding: utf-8 -*-

##------Setup your directories-----

localstoriespath = '/home/rbckman/stories/'
localimagepath = '/home/rbckman/Pictures/'
server = "rob@robinbackman.com"
serverstoriespath = "/srv/www/robinbackman.com/stories/"
serverimagepath = "/srv/www/robinbackman.com/images/"
updwebpath = "/srv/www/robinbackman.com/updweb.py"

##-------Other setups--------------

height = 2  ## how many rows, set this low if your on a mobile with non physical keyboard, good value = 2
